/**

Print a pyramid!

    *
   ***
  *****
 *******
*********

ACCEPTANCE CRITERIA:

Write a script to output pyramid of given size to the console (with leading spaces).

*/

function pyramid(size = 5) {
  for (let i = 0; i < size; i++) {
    let line = '';

    for (let j = 0; j < size - i - 1; j++) {
      line += ' ';
    }
    for (let k = 0; k < 2 * i + 1; k++) {
      line += '*';
    }

    console.log(line);
  }
}

pyramid(5);
